// import the required packages from the AWS SDK for Java
import redis.clients.jedis.*;
import java.util.*;

public class RedisSample {
    
    public static void main(String args[]) {
        new RedisSample().test();
    }

    public void log(Object msg) {
        System.out.println("" + msg);
    }

    public void test() {
        log("Inside test()");
        try {

            log("Trying method-1");
            Jedis jedis = new Jedis("test-redis-cache.xc4xdb.ng.0001.use1.cache.amazonaws.com:6379");
            jedis.set("foo", "bar");
            String value = jedis.get("foo");
            log("Retrieved value for key: foo, Value=" + value);

            log("Trying method-2");
            Set<HostAndPort> jedisClusterNodes = new HashSet<HostAndPort>();
            //Jedis Cluster will attempt to discover cluster nodes automatically
            jedisClusterNodes.add(new HostAndPort("test-redis-cache.xc4xdb.ng.0001.use1.cache.amazonaws.com", 6379));
            JedisCluster jc = new JedisCluster(jedisClusterNodes);
            jc.set("foo2", "bar2");
            log("Set value: foo2=bar2");
            String value2 = jc.get("foo2");
            log("Retrieved value for key: foo2, Value=" + value2);

        } catch(Exception ex) {
            log("Exception in test()");
            ex.printStackTrace();
        }
        log("Exiting test()");
    }
}
