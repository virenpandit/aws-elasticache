// import the required packages from the AWS SDK for Java
import net.spy.memcached.*;
import java.util.*;
import java.net.*;

public class MemcachedSample {
    
    public static void main(String args[]) {
        new MemcachedSample().test();
    }

    public void log(Object msg) {
        System.out.println("" + msg);
    }

    public void test() {
        log("Inside test()");
        try {
            MemcachedClient c=new MemcachedClient(
                new InetSocketAddress("test-vpc-elcache.xc4xdb.cfg.use1.cache.amazonaws.com", 11211));

            // Store a value (async) for one hour
            c.set("someKey", 3600, "someObject");
            log("Object with key: someKey put in case successfully!");
            // Retrieve a value (synchronously).
            Object myObject=c.get("someKey");
            log("Retrieved object from ELCache: " + myObject);
            c.shutdown();

        } catch(Exception ex) {
            log("Exception in test()");
            ex.printStackTrace();
        }
        log("Exiting test()");
    }
}
